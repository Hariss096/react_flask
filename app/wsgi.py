from user_auth import start_server
from user_auth import routes
from user_auth import auth

app = start_server()

if __name__ == "__main__":
    app.run(debug=True)
