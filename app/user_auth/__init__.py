from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
login_manager = LoginManager()
app = Flask(__name__)
CORS(app)


def start_server():
    global app
    # Load config from file
    app.config.from_object('config.Config')

    db.init_app(app)
    migrate = Migrate(app, db)
    login_manager.init_app(app)

    with app.app_context():
        # app.register_blueprint(routes.main_bp)
        # app.register_blueprint(auth.auth_bp)

        db.create_all()

        # if app.config['FLASK_ENV'] == 'development':
        #     compile_assets(app)

        return app
