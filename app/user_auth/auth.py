import os
import logging
from . import login_manager, app
from .models import db, User, UserDocs
from urllib.parse import urlparse, urljoin
from werkzeug.utils import secure_filename
from .forms import SignupForm, LoginForm, FilesForm
from flask import redirect, request, url_for, flash, abort
from flask_login import login_required, logout_user, current_user, login_user

ALLOWED_EXTENSION = {'pdf'}


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return {'user': current_user.is_authenticated}

    form = LoginForm(meta={'csrf': False})
    # Validate login
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(password=form.password.data):
            login_user(user)
            next_page = request.args.get('next')
            if not is_safe_url(next_page):
                return abort(400)
            return {'user': current_user.is_authenticated}
        return abort(401)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    # Signup logic
    form = SignupForm(meta={'csrf': False})
    if request.method == 'POST' and form.validate_on_submit():
        existing_user = User.query.filter_by(email=form.email.data).first()
        if existing_user is None:
            user = User(
                username=form.username.data,
                email=form.email.data
            )
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()  # Create new user
            login_user(user)  # Log in as newly created user
            return {'user': current_user.is_authenticated}


@app.route('/dashboard', methods=["GET"])
@login_required
def user_dashboard():
    return UserDocs.query.filter(UserDocs.user_id == current_user.id).all()


@app.route('/files', methods=["GET", "POST"])
@login_required
def files():
    form = FilesForm()
    if request.method == "POST" and form.validate_on_submit():
        if 'file' not in request.files:
            return abort(500)
        file = request.files['file']
        if file.filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSION:
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return f'Successfully uploaded_file: {filename}'
        return abort(400)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('/'))


@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in upon page loading"""
    if user_id is not None:
        return User.query.get(user_id)
    return None


@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    flash('Please login to view this page!')
    return redirect(url_for('login'))