from flask_wtf import FlaskForm, file
from wtforms import StringField, SubmitField, PasswordField, IntegerField
from wtforms.validators import DataRequired, Length, Email, EqualTo


class SignupForm(FlaskForm):
    username = StringField('Username',
                           validators=[DataRequired()])
    email = StringField('Email',
                        validators=[Length(min=6),
                                    Email(message='Enter a valid email.'), DataRequired()])
    password = PasswordField('Password',
                             validators=[DataRequired(),
                                         Length(min=6, message='Select a stronger password.')])


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(),
                                             Email(message="Enter valid Email address")])
    password = PasswordField('Password', validators=[DataRequired()])
    submit = SubmitField('Login')


class FilesForm(FlaskForm):
    user_id = IntegerField()
    user_files = file.FileField()
