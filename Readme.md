A mono-repo containing Flask based backend and React based frontend. Following configuration is required to run the project successfully:


**Install dependencies:**
-  Backend (/app)
    -  cd /app && python -m virtualenv flaskenv (recommended but not absolutely necessary)
    -  cd /app && pip install -r requirements.txt


-  Frontend (/frontend)
    - cd /frontend && npm install


- frontend is bootstrapped from create-react-app.
- .env file will have to be populated. An example .env file is uploaded for guidance.

**SQLITE DB Setup**

Install sqlite using:
   -  brew (mac)
   -  apt-get (linux)

Sqlite3 initial setup:
- Fire the following command on terminal in ${BASE_PATH}/api folder:
    - sqlite3 users.db

