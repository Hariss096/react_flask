import React from 'react';
import { Redirect, Route, withRouter } from 'react-router-dom';


const UnauthedRoute = ({ component: Component, ...rest}) => {
    return(
        <Route
            {...rest}
            render={(props) => {
                if (!window.localStorage.getItem('currentUser')) {
                    return <Component {...props} />;
                } else if (rest.redirect) {
                    return <Redirect to='/' />;
                }
            }}
        />
    );
};

UnauthedRoute.defaultProps = {
    redirect: true,
};

export default withRouter(UnauthedRoute)