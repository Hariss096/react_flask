import React, { useState } from 'react'
import { withRouter } from 'react-router-dom';
import { TableContainer,
    Table,
    TableBody,
    TableHead,
    TableCell,
    TableRow,
    Paper,
    Input,
    Button } from '@material-ui/core';


async function uploadFile(payload) {
    var settings = {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        body: JSON.stringify(payload)
      }
    //   const response = await fetch('http://localhost:5000/files', settings);
}

function Dashboard() {
    const [file, setFile] = useState();
    const onChangeHandler=(event)=>{
        console.log(event.target.files[0])
        setFile()
    }

    const onFileUploadHandler= () => {
        const data = new FormData()
        data.append('file')
    }

    return(
        <TableContainer component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                    <TableCell></TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <Input type="file" align="left" onChange={onChangeHandler}></Input>
                        <Button variant="contained" color="secondary" onClick={onFileUploadHandler}>Upload to server</Button>
                        <Button variant="contained" color="secondary" align="right">View</Button>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default withRouter(Dashboard)