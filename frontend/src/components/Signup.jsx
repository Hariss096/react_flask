import React, { useState } from 'react';
import { TextField, Grid, Container, Button, makeStyles } from '@material-ui/core';
import { Redirect } from 'react-router-dom';


const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(3),
  },
}));

async function signupUser(credentials) {
  var settings = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  };
  try {
    const response = await fetch('http://localhost:5000/signup', settings);
    const data = await response.json();
    return data;
  } catch(err) {
    return err;
  }
 }

export default function Signup() {
    const classes = useStyles();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [username, setUsername] = useState(false);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const handleSubmit = async e => {
      e.preventDefault();
      var isLoggedIn = await signupUser({
        username,
        email,
        password
      });
      sessionStorage.setItem('currentUser', isLoggedIn['user'])
      setIsLoggedIn(isLoggedIn['user']);
    }
  
    return (
      <Container className={classes.container} maxWidth="xs">
        <form onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
              <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Username"
                    name="username"
                    size="small"
                    variant="outlined"
                    onChange={e => setUsername(e.target.value)} />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Email"
                    name="email"
                    size="small"
                    variant="outlined"
                    onChange={e => setEmail(e.target.value)} />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Password"
                    name="password"
                    size="small"
                    type="password"
                    variant="outlined"
                    onChange={e => setPassword(e.target.value)} />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Button color="secondary" fullWidth type="submit" variant="contained">
                Sign Up
              </Button>
            </Grid>
          </Grid>
          {isLoggedIn ? <Redirect to="/dashboard" />: ''}
        </form>
      </Container>
    );
  };