import React, { useState } from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { Button, Grid, Container, TextField, makeStyles } from '@material-ui/core';



const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(3),
  },
}));


async function loginUser(credentials) {
  var settings = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  };
  try {
    const response = await fetch('http://localhost:5000/login', settings);
    const data = await response.json();
    return data;
  } catch(err) {
    return err;
  }
 }

function Login() {
  const classes = useStyles();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();
    var isLoggedIn = await loginUser({
      email,
      password
    });
    sessionStorage.setItem('currentUser', isLoggedIn['user'])
    setIsLoggedIn(isLoggedIn['user']);
  }

  return(
    <Container className={classes.container} maxWidth="xs">
        <form onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Email"
                    name="email"
                    size="small"
                    variant="outlined" 
                    onChange={e => setEmail(e.target.value)}/>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    fullWidth
                    label="Password"
                    name="password"
                    size="small"
                    type="password"
                    variant="outlined"
                    onChange={e => setPassword(e.target.value)}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Button color="secondary" fullWidth type="submit" variant="contained">
                Log in
              </Button>
            </Grid>
          </Grid>
          {isLoggedIn ? <Redirect to="/dashboard" />: ''}
        </form>
      </Container>
  )
}

export default withRouter(Login)