import React, { Component } from 'react';
import { Route, Redirect, withRouter } from 'react-router-dom';


class AuthedRoute extends Component {
    render() {
        const { component, ...rest } = this.props;

        return (
            <Route
                {...rest}
                render={(props) => {
                    if (!sessionStorage.getItem('currentUser')) {
                        if (this.props.redirect) {
                            return <Redirect to="/login" />;
                        }
                    }
                    return <Route to={this.props.location.pathname} component={component}/>;
                }}
            />
        );
    }
}

AuthedRoute.defaultProps = {
    redirect: true,
};


export default withRouter(AuthedRoute)