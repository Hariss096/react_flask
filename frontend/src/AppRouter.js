import React from 'react'
import Login from './components/Login'
import Signup from './components/Signup'
import Dashboard from './components/Dashboard'
import { Route, Switch } from 'react-router-dom';

import AuthedRoute from './components/AuthedRoute';
import UnauthedRoute from './components/UnauthedRoute';


export default function AppRouter() {
    return(
        <Switch>
            <Route exact path="/" component={Login} />
            <UnauthedRoute path="/signup" component={Signup} />
            <UnauthedRoute path="/login" component={Login} />
            <AuthedRoute path="/dashboard" component={Dashboard} />
        </Switch>
    )
}